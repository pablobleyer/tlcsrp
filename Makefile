#
# TLCSRP Makefile
#

TOMMATH = c:/Work/zhk/lib/libtommath
TOMCRYPT = c:/Work/zhk/lib/libtomcrypt
CLIB = c:/Work/zhk/bld/x86

CINC = $(TOMMATH) $(TOMCRYPT)/src/headers .

COBJ = tlcsrp tlcsrp_win

INCLUDES = $(addprefix -I,$(CINC))
LIBPATH = $(addprefix -L,$(CLIB))

ifeq ($(BUILD), msvc)

TOOLDIR = C:/VisualStudio/VC
TOOLBIN = $(TOOLDIR)/bin/

# dumpbin executable
DUMPBIN = $(TOOLBIN)dumpbin
# lib executable
VCLIB = $(TOOLBIN)lib

CC = $(TOOLBIN)cl

else

# TOOLDIR = C:/MinGW64/mingw64
TOOLDIR = C:/MinGW
TOOLBIN = $(TOOLDIR)/bin/

CFLAGS = -std=c11 -g -O3 -Wall $(INCLUDES) # -DSRAND_ZERO -DDEBUG # -Wl,--kill-at
LDLIBS = $(LIBPATH) -ltommath -ltomcrypt # -L. -ltcsrp

# pexports executable (Note: GCC must be in the executables path)
PEXPORTS = $(TOOLBIN)pexports
# dlltool executable
DLLTOOL = $(TOOLBIN)dlltool

CC = $(TOOLBIN)gcc

OBJ =  $(addsuffix .o,$(COBJ))

endif

.PHONY: clean distclean

all: tlcsrp_test

# G++ needed @x function suffix to link executable, thus header file is provided and later removed with dlltool
#libtlcsrp.a:
#	$(PEXPORTS) -h $tlcsrp.h $*.dll > $*.def
#	$(DLLTOOL) -k -d $*.def -l $@

libtlcsrp.a: libtlcsrp.a($(OBJ))

tlcsrp.dll: $(OBJ)
	$(CC) -shared $^ -o $@ $(LDLIBS) # -Wl,--out-implib,lib$*.a

tlcsrp.lib:
#	$(DUMPBIN) /exports $*.dll > $*.def
	$(PEXPORTS) $*.dll > $*.def
	$(VCLIB) /machine:x64 /def:$*.def /OUT:$@

tlcsrp_test: tlcsrp.o csrp_win.o

clean:
	$(RM) *.o *.def *.a *.exe *.lib

distclean: clean
	$(RM) *.bak
