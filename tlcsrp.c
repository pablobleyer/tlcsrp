/** @file
	Customization from csrp srp.c to use TomMath and TomCrypt
	@author Pablo Bleyer <pablo@gozendo.com>
*/

/*
 * Secure Remote Password 6a implementation
 * Original copyright:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Zendo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#if defined(_WIN32)
	#include <Windows.h>
	#include <Wincrypt.h>
#elif defined(__CC_ARM)
#else
	#include <sys/time.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "tlcsrp.h"

/* Flag that the random number generator has been initialized. */
static bool g_initialized = false;

struct NGHex
{
	const char *n_hex;
	const char *g_hex;
};

/* All constants here were pulled from Appendix A of RFC 5054 */
static struct NGHex global_Ng_constants[] = {
#ifdef SRP_NG_1024
	{ /* 1024 */
		"EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C"
		"9C256576D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE4"
		"8E495C1D6089DAD15DC7D7B46154D6B6CE8EF4AD69B15D4982559B29"
		"7BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA9A"
		"FD5138FE8376435B9FC61D2FC0EB06E3",
		"2"
	},
#endif
#ifdef SRP_NG_1536
	{ /* 1536 */
		"9DEF3CAFB939277AB1F12A8617A47BBBDBA51DF499AC4C80BEEEA961"
		"4B19CC4D5F4F5F556E27CBDE51C6A94BE4607A291558903BA0D0F843"
		"80B655BB9A22E8DCDF028A7CEC67F0D08134B1C8B97989149B609E0B"
		"E3BAB63D47548381DBC5B1FC764E3F4B53DD9DA1158BFD3E2B9C8CF5"
		"6EDF019539349627DB2FD53D24B7C48665772E437D6C7F8CE442734A"
		"F7CCB7AE837C264AE3A9BEB87F8A2FE9B8B5292E5A021FFF5E91479E"
		"8CE7A28C2442C6F315180F93499A234DCF76E3FED135F9BB",
		"2"
	},
#endif
#ifdef SRP_NG_2048
	{ /* 2048 */
		"AC6BDB41324A9A9BF166DE5E1389582FAF72B6651987EE07FC319294"
		"3DB56050A37329CBB4A099ED8193E0757767A13DD52312AB4B03310D"
		"CD7F48A9DA04FD50E8083969EDB767B0CF6095179A163AB3661A05FB"
		"D5FAAAE82918A9962F0B93B855F97993EC975EEAA80D740ADBF4FF74"
		"7359D041D5C33EA71D281E446B14773BCA97B43A23FB801676BD207A"
		"436C6481F1D2B9078717461A5B9D32E688F87748544523B524B0D57D"
		"5EA77A2775D2ECFA032CFBDBF52FB3786160279004E57AE6AF874E73"
		"03CE53299CCC041C7BC308D82A5698F3A8D0C38271AE35F8E9DBFBB6"
		"94B5C803D89F7AE435DE236D525F54759B65E372FCD68EF20FA7111F"
		"9E4AFF73",
		"2"
	},
#endif
#ifdef SRP_NG_3072
	{ /* 3072 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A93AD2CAFFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_4096
	{ /* 4096 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199"
		"FFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_6144
	{ /* 6144 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934028492"
		"36C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406"
		"AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918"
		"DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B33205151"
		"2BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03"
		"F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97F"
		"BEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AA"
		"CC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58B"
		"B7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632"
		"387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E"
		"6DCC4024FFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_8192
	{ /* 8192 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934028492"
		"36C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406"
		"AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918"
		"DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B33205151"
		"2BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03"
		"F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97F"
		"BEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AA"
		"CC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58B"
		"B7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632"
		"387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E"
		"6DBE115974A3926F12FEE5E438777CB6A932DF8CD8BEC4D073B931BA"
		"3BC832B68D9DD300741FA7BF8AFC47ED2576F6936BA424663AAB639C"
		"5AE4F5683423B4742BF1C978238F16CBE39D652DE3FDB8BEFC848AD9"
		"22222E04A4037C0713EB57A81A23F0C73473FC646CEA306B4BCBC886"
		"2F8385DDFA9D4B7FA2C087E879683303ED5BDD3A062B3CF5B3A278A6"
		"6D2A13F83F44F82DDF310EE074AB6A364597E899A0255DC164F31CC5"
		"0846851DF9AB48195DED7EA1B1D510BD7EE74D73FAF36BC31ECFA268"
		"359046F4EB879F924009438B481C6CD7889A002ED5EE382BC9190DA6"
		"FC026E479558E4475677E9AA9E3050E2765694DFC81F56E880B96E71"
		"60C980DD98EDD3DFFFFFFFFFFFFFFFFF",
		"13"
	},
#endif
 	{0, 0} /* null sentinel */
};

#ifdef DEBUG
static void
print_mp(const mp_int *m)
{
	unsigned i = 0;
	char buf[8*1024];

	mp_toradix(m, buf, 16);

	while (i < USED(m))
	{
		unsigned n = USED(m) - i;
		n = (n < 16) ? n : 16;
		printf("%04x: ", i);
		for (unsigned j = 0; j < n; ++j)
			printf ("%c%c ", buf[i + 2*j], buf[i + 2*j+1]);
		printf("\n");
		i += n;
	}
}
#endif

static bool
init_ng(SrpNgConstant *ng, SrpNgType ng_type, const char *n_hex, const char *g_hex)
{
	if (!ng || mp_init_multi(&ng->N, &ng->g, NULL) != MP_OKAY)
		return false;

	if (ng_type != srp_Ng_CUSTOM)
	{
		n_hex = global_Ng_constants[ng_type].n_hex;
		g_hex = global_Ng_constants[ng_type].g_hex;
	}

	if (!n_hex || !g_hex)
		return false;

	mp_read_radix(&ng->N, n_hex, 16);
	mp_read_radix(&ng->g, g_hex, 16);

	return true;
}

static void
clear_ng(SrpNgConstant *ng)
{
	if (!ng)
		return;

	mp_clear(&ng->N);
	mp_clear(&ng->g);
}

static int
hash_init(SrpHashAlgorithm alg, hash_state *c)
{
	switch (alg)
	{
	case srp_SHA1: return sha1_init(c);
	case srp_SHA224: return sha224_init(c);
	case srp_SHA256: return sha256_init(c);
	case srp_SHA384: return sha384_init(c);
	case srp_SHA512: return sha512_init(c);
	default: return CRYPT_INVALID_HASH;
	}
}

static int
hash_update(SrpHashAlgorithm alg, hash_state *c, const void *data, size_t len)
{
	switch (alg)
	{
	case srp_SHA1: return sha1_process(c, data, len);
	case srp_SHA224: return sha224_process(c, data, len);
	case srp_SHA256: return sha256_process(c, data, len);
	case srp_SHA384: return sha384_process(c, data, len);
	case srp_SHA512: return sha512_process(c, data, len);
	default: return CRYPT_INVALID_HASH;
	}
}

static int
hash_final(SrpHashAlgorithm alg, hash_state *c, uint8_t *md)
{
	switch (alg)
	{
	case srp_SHA1: return sha1_done(c, md);
	case srp_SHA224: return sha224_done(c, md);
	case srp_SHA256: return sha256_done(c, md);
	case srp_SHA384: return sha384_done(c, md);
	case srp_SHA512: return sha512_done(c, md);
	default: return CRYPT_INVALID_HASH;
	}
}

static uint8_t *
hash(SrpHashAlgorithm alg, const uint8_t *d, size_t n, uint8_t *md)
{
	hash_state c;

	if (hash_init(alg, &c) != CRYPT_OK
		|| hash_update(alg, &c, d, n) != CRYPT_OK
		|| hash_final(alg, &c, md) != CRYPT_OK)
		return NULL;
	else
		return md;
}

static int
hash_length(SrpHashAlgorithm alg)
{
	switch (alg)
	{
	case srp_SHA1  : return SHA1_DIGEST_LENGTH;
	case srp_SHA224: return SHA224_DIGEST_LENGTH;
	case srp_SHA256: return SHA256_DIGEST_LENGTH;
	case srp_SHA384: return SHA384_DIGEST_LENGTH;
	case srp_SHA512: return SHA512_DIGEST_LENGTH;
	default: return CRYPT_INVALID_HASH;
	}
}

static bool
H_nn(mp_int *r, SrpHashAlgorithm alg, const mp_int *n1, const mp_int *n2)
{
	uint8_t buff[SHA512_DIGEST_LENGTH];
	int len_n1 = mp_unsigned_bin_size((mp_int *)n1);
	int len_n2 = mp_unsigned_bin_size((mp_int *)n2);
	int nbytes = len_n1 + len_n2;
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		return false;

	if (mp_to_unsigned_bin((mp_int *)n1, bin) != MP_OKAY
		|| mp_to_unsigned_bin((mp_int *)n2, bin + len_n1) != MP_OKAY)
	{
		free(bin);
		return false;
	}

	uint8_t *h = hash(alg, bin, nbytes, buff);
	free(bin);

	if (!h)
		return false;

	return (mp_read_unsigned_bin(r, buff, hash_length(alg)) == MP_OKAY);
}

static bool
H_ns(mp_int *r, SrpHashAlgorithm alg, const mp_int *n, const uint8_t *bytes, int len_bytes)
{
	uint8_t buff[SHA512_DIGEST_LENGTH];
	int len_n = mp_unsigned_bin_size((mp_int *)n);
	int nbytes = len_n + len_bytes;
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		 return false;

	if (mp_to_unsigned_bin((mp_int *)n, bin) != MP_OKAY)
	{
		free(bin);
		return false;
	}

	memcpy(bin + len_n, bytes, len_bytes);
	uint8_t *h = hash(alg, bin, nbytes, buff);
	free(bin);

	if (!h)
		return false;

	return (mp_read_unsigned_bin(r, buff, hash_length(alg)) == MP_OKAY);
}

static bool
calculate_x(mp_int *r, SrpHashAlgorithm alg, const mp_int *salt, const char *username, const char *password, int password_len)
{
	uint8_t ucp_hash[SHA512_DIGEST_LENGTH];
	hash_state ctx;

	if (hash_init(alg, &ctx) != CRYPT_OK
		|| hash_update(alg, &ctx, username, strlen(username)) != CRYPT_OK
		|| hash_update(alg, &ctx, ":", 1)  != CRYPT_OK
		|| hash_update(alg, &ctx, password, password_len) != CRYPT_OK
		|| hash_final(alg, &ctx, ucp_hash) != CRYPT_OK)
		return false;

	return H_ns(r, alg, salt, ucp_hash, hash_length(alg));
}

static bool
update_hash_n(SrpHashAlgorithm alg, hash_state *ctx, const mp_int *n)
{
	unsigned long len = mp_unsigned_bin_size((mp_int *)n);
	uint8_t *n_bytes = (uint8_t *)malloc(len);

	if (!n_bytes)
		 return false;

	bool r = mp_to_unsigned_bin((mp_int *)n, n_bytes) == MP_OKAY
		&& hash_update(alg, ctx, n_bytes, len) == CRYPT_OK;

	free(n_bytes);

	return r;
}

static bool
hash_num(SrpHashAlgorithm alg, const mp_int *n, uint8_t *dest)
{
	int nbytes = mp_unsigned_bin_size((mp_int *)n);
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		return false;

	bool r = mp_to_unsigned_bin((mp_int *)n, bin) == MP_OKAY
		&& hash(alg, bin, nbytes, dest) != NULL;

	free(bin);

	return r;
}

static bool
calculate_M(
	SrpHashAlgorithm alg, SrpNgConstant *ng, uint8_t *dest, const char *I,
	const mp_int *s, const mp_int *A, const mp_int *B, const uint8_t *K
)
{
	uint8_t H_N[SHA512_DIGEST_LENGTH];
	uint8_t H_g[SHA512_DIGEST_LENGTH];
	uint8_t H_I[SHA512_DIGEST_LENGTH];
	uint8_t H_xor[SHA512_DIGEST_LENGTH];
	hash_state ctx;
	int hash_len = hash_length(alg);

	if (!hash_num(alg, &ng->N, H_N)
		|| !hash_num(alg, &ng->g, H_g)
		|| hash(alg, (const uint8_t *)I, strlen(I), H_I) == NULL)
		return false;

	for (int i = 0; i < hash_len; ++i)
		H_xor[i] = H_N[i] ^ H_g[i];

	return hash_init(alg, &ctx) == CRYPT_OK
		&& hash_update(alg, &ctx, H_xor, hash_len) == CRYPT_OK
		&& hash_update(alg, &ctx, H_I, hash_len) == CRYPT_OK
		&& update_hash_n(alg, &ctx, s)
		&& update_hash_n(alg, &ctx, A)
		&& update_hash_n(alg, &ctx, B)
		&& hash_update(alg, &ctx, K, hash_len) == CRYPT_OK
		&& hash_final(alg, &ctx, dest) == CRYPT_OK;
}

static bool
calculate_H_AMK(SrpHashAlgorithm alg, uint8_t *dest, const mp_int *A, const uint8_t *M, const uint8_t *K)
{
	hash_state ctx;

	return hash_init(alg, &ctx) == CRYPT_OK
		&& update_hash_n(alg, &ctx, A)
		&& hash_update(alg, &ctx, M, hash_length(alg)) == CRYPT_OK
		&& hash_update(alg, &ctx, K, hash_length(alg)) == CRYPT_OK
		&& hash_final(alg, &ctx, dest) == CRYPT_OK;
}

static void
init_random()
{
	if (g_initialized)
		return;

	// Device-dependend random initialization
	g_initialized = srp_random_init();
}

/*
	Exported Functions
*/

void
srp_random_seed(const uint8_t *random_data, int data_length)
{
	g_initialized = true;

	if (random_data)
		switch (data_length)
		{
		case 1: srand(*(uint8_t *)random_data); break;
		case 2: srand(*(uint16_t *)random_data); break;
		default: srand(*(uint32_t *)random_data); break;
		}
		// RAND_seed(random_data, data_length);
}

bool
srp_create_salted_verification_key(
	SrpHashAlgorithm alg, SrpNgType ng_type,
	const char *username, const char *password, int len_password,
	const uint8_t **bytes_s, int *len_s, const uint8_t **bytes_v,
	int *len_v, const char *n_hex, const char *g_hex
)
{
	SrpNgConstant ng;
	mp_int s, v, x;
	int l_s, l_v;
	uint8_t *b_v, *b_s;

	if (!init_ng(&ng, ng_type, n_hex, g_hex))
		return false;

	if (mp_init_multi(&s, &v, &x, NULL) != MP_OKAY)
	{
		clear_ng(&ng);
		return false;
	}

	init_random();

	bool r = false;
	// BN_rand(s, 32, -1, 0); // 32-bits, msb can be zero, odd number
	if (mp_rand(&s, 32) != MP_OKAY
		|| !calculate_x(&x, alg, &s, username, password, len_password)
		|| mp_exptmod(&ng.g, &x, &ng.N, &v) != MP_OKAY) // v = g^x % N
		goto cleanup_and_exit;

#ifdef DEBUG
	printf("s[%d]:\n", USED(&s));
	print_mp(&s);
	printf("\n");

	printf("x[%d]:\n", USED(&x));
	print_mp(&x);
	printf("\n");

	printf("v[%d]:\n", USED(&v));
	print_mp(&v);
	printf("\n");
#endif

	l_s = mp_unsigned_bin_size(&s);
	l_v = mp_unsigned_bin_size(&v);

	b_v = (uint8_t *)malloc(l_v);
	b_s = (uint8_t *)malloc(l_s);
	if (b_v == NULL
		|| b_s == NULL
		|| mp_to_unsigned_bin(&s, b_s) != MP_OKAY
		|| mp_to_unsigned_bin(&v, b_v) != MP_OKAY)
	{
		if (b_v)
			free(b_v);
		if (b_s)
			free(b_s);
		goto cleanup_and_exit;
	}

	if (len_s)
		*len_s = l_s;

	if (len_v)
		*len_v = l_v;

	if (bytes_s)
		*bytes_s = b_s;
	else
		free(b_s);

	if (bytes_v)
		*bytes_v = b_v;
	else
		free(b_v);

	r = true;

cleanup_and_exit:
	mp_clear_multi(&s, &v, &x, NULL);
	clear_ng(&ng);

	return r;
}

/* Out: bytes_B, len_B.
 	On failure, bytes_B will be set to NULL and len_B will be set to 0
*/
bool
srp_verifier_init(
	SrpVerifier *ver, SrpHashAlgorithm alg, SrpNgType ng_type, const char *username,
	const uint8_t *bytes_s, int len_s, const uint8_t *bytes_v, int len_v,
	const uint8_t *bytes_A, int len_A, const uint8_t **bytes_B, int *len_B,
	const char *n_hex, const char *g_hex
)
{
	mp_int s, v, A, u, B, S, b, k, tmp1, tmp2;
	SrpNgConstant *ng = NULL;
	int ulen;

	if (!ver || mp_init_multi(&s, &v, &A, &u, &B, &S, &b, &k, &tmp1, &tmp2, NULL) != MP_OKAY)
		return false;

	bool r = false;
	if (mp_read_unsigned_bin(&s, bytes_s, len_s) != MP_OKAY
		|| mp_read_unsigned_bin(&v, bytes_v, len_v) != MP_OKAY
		|| mp_read_unsigned_bin(&A, bytes_A, len_A) != MP_OKAY)
		goto cleanup_and_exit;

	ulen = strlen(username) + 1;
	ver->username = (char *)malloc(ulen);
	if (!ver->username)
		 goto cleanup_and_exit;
	memcpy(ver->username, username, ulen);

	if (!init_ng(&ver->ng, ng_type, n_hex, g_hex))
		goto cleanup_and_exit;

	ng = &ver->ng;
	init_random();

	ver->hash_alg = alg;
	ver->authenticated = false;

	/* SRP-6a safety check */
	if (mp_mod(&A, &ng->N, &tmp1) != MP_OKAY || mp_iszero(&tmp1))
		goto cleanup_and_exit;

	// BN_rand(b, 256, -1, 0);
	if (mp_rand(&b, 256) != MP_OKAY
		|| !H_nn(&k, alg, &ng->N, &ng->g)
		/* B = kv + g^b */
		|| mp_mul(&k, &v, &tmp1) != MP_OKAY
		|| mp_exptmod(&ng->g, &b, &ng->N, &tmp2) != MP_OKAY
		|| mp_add(&tmp1, &tmp2, &B) != MP_OKAY
		|| !H_nn(&u, alg, &A, &B)
		/* S = (A *(v^u)) ^ b */
		|| mp_exptmod(&v, &u, &ng->N, &tmp1) != MP_OKAY
		|| mp_mul(&A, &tmp1, &tmp2) != MP_OKAY
		|| mp_exptmod(&tmp2, &b, &ng->N, &S) != MP_OKAY
		|| !hash_num(alg, &S, ver->session_key)
		|| !calculate_M(alg, ng, ver->M, username, &s, &A, &B, ver->session_key)
		|| !calculate_H_AMK(alg, ver->H_AMK, &A, ver->M, ver->session_key))
		goto cleanup_and_exit;

	int l_B = mp_unsigned_bin_size(&B);
	uint8_t *b_B = (uint8_t *)malloc(l_B);
	if (!b_B)
		goto cleanup_and_exit;

	if (mp_to_unsigned_bin(&B, b_B) != MP_OKAY)
		goto cleanup_and_exit;
	ver->bytes_B = b_B;

	if (len_B)
		*len_B = l_B;

	if (bytes_B)
		*bytes_B = b_B;

	r = true;

cleanup_and_exit:
 	if (!r)
 	{
 		if (ver->username)
			free(ver->username);
		ver->username = NULL;

		if (ng)
			clear_ng(ng);
 	}
	mp_clear_multi(&s, &v, &A, &u, &B, &S, &b, &k, &tmp1, &tmp2, NULL);

	return r;
}

void
srp_verifier_clear(SrpVerifier *ver)
{
	if (!ver)
		return;

	clear_ng(&ver->ng);
	free(ver->username);
	free(ver->bytes_B);
	memset(ver, 0, sizeof(SrpVerifier));
}

bool
srp_verifier_is_authenticated(SrpVerifier * ver)
{
	return ver->authenticated;
}

const char *
srp_verifier_get_username(SrpVerifier * ver)
{
	return ver->username;
}

const uint8_t *
srp_verifier_get_session_key(SrpVerifier *ver, int *key_length)
{
	if (key_length)
		*key_length = hash_length(ver->hash_alg);
	return ver->session_key;
}

int
srp_verifier_get_session_key_length(SrpVerifier *ver)
{
	return hash_length(ver->hash_alg);
}

/* user_M must be exactly SHA512_DIGEST_LENGTH bytes in size */
bool
srp_verifier_verify_session(SrpVerifier *ver, const uint8_t *user_M, const uint8_t **bytes_HAMK)
{
	bool r = memcmp(ver->M, user_M, hash_length(ver->hash_alg)) == 0;

	if (r)
		ver->authenticated = true;

	if (bytes_HAMK)
		*bytes_HAMK = (r) ? ver->H_AMK : NULL;

	return r;
}

/* */

bool
srp_user_init(
	SrpUser *usr, SrpHashAlgorithm alg, SrpNgType ng_type,
	const char *username, const char *bytes_password, int len_password,
	const char *n_hex, const char *g_hex
)
{
	init_random();

	if (!usr)
		return false;

	if (!init_ng(&usr->ng, ng_type, n_hex, g_hex))
		return false;

	if (mp_init_multi(&usr->a, &usr->A, &usr->S, NULL) != MP_OKAY)
	{
		clear_ng(&usr->ng);
		return false;
	}

	usr->hash_alg = alg;
	int ulen = strlen(username) + 1;
	usr->username = (char *)malloc(ulen);
	usr->password = (char *)malloc(len_password);
	usr->password_len = len_password;

	if (!usr->username || !usr->password)
		 goto err_exit;

	memcpy((char *)usr->username, username, ulen);
	memcpy((char *)usr->password, bytes_password, len_password);

	usr->authenticated = false;
	usr->bytes_A = NULL;

	return true;

 err_exit:
	 if (usr->password)
	 {
		 memset(usr->password, 0, usr->password_len);
		 free(usr->password);
	 }

	 if (usr->username)
	 {
		 memset(usr->username, 0, ulen);
		 free(usr->username);
	 }

	 mp_clear_multi(&usr->a, &usr->A, &usr->S, NULL);
	 clear_ng(&usr->ng);

	 return false;
}

void
srp_user_clear(SrpUser *usr)
{
	if (!usr)
		return;

	mp_clear_multi(&usr->a, &usr->A, &usr->S, NULL);
	clear_ng(&usr->ng);

	if (usr->username)
		free(usr->username);

	if (usr->password)
	{
		memset(usr->password, 0, usr->password_len);
		free(usr->password);
	}

	if (usr->bytes_A)
		free(usr->bytes_A);

	memset(usr, 0, sizeof(SrpUser));
}

bool
srp_user_is_authenticated(SrpUser *usr)
{
	return usr->authenticated;
}


const char *
srp_user_get_username(SrpUser *usr)
{
	return usr->username;
}

const uint8_t *
srp_user_get_session_key(SrpUser *usr, int *key_length)
{
	if (key_length)
		*key_length = hash_length(usr->hash_alg);

	return usr->session_key;
}

int
srp_user_get_session_key_length(SrpUser *usr)
{
	return hash_length(usr->hash_alg);
}

/* Output: username, bytes_A, len_A */
bool
srp_user_start_authentication(
	SrpUser *usr, const char **username,
	const uint8_t **bytes_A, int *len_A
)
{
	// BN_rand(usr->a, 256, -1, 0);
	if (mp_rand(&usr->a, 256) != MP_OKAY
		|| mp_exptmod(&usr->ng.g, &usr->a, &usr->ng.N, &usr->A) != MP_OKAY)
		return false;

	int l_A = mp_unsigned_bin_size(&usr->A);
	uint8_t *b_A = (uint8_t *)malloc(l_A);
	if (!b_A)
		return false;

	if (mp_to_unsigned_bin(&usr->A, b_A) != MP_OKAY)
	{
		free(b_A);
		return false;
	}

	usr->bytes_A = b_A;

	if (username)
		*username = usr->username;

	if (bytes_A)
		*bytes_A = b_A;

	if (len_A)
		*len_A = l_A;

	return true;
}

/* Output: bytes_M. Buffer length is SHA512_DIGEST_LENGTH */
bool
srp_user_process_challenge(SrpUser * usr,
	const uint8_t *bytes_s, int len_s,
	const uint8_t *bytes_B, int len_B,
	const uint8_t **bytes_M, int *len_M
)
{
	mp_int s, B, u, x, k, v, tmp1, tmp2, tmp3;

	if (mp_init_multi(&s, &B, &u, &x, &k, &v, &tmp1, &tmp2, &tmp3, NULL) != MP_OKAY)
		return false;

	bool r = false;
	if (mp_read_unsigned_bin(&s, bytes_s, len_s) != MP_OKAY
		|| mp_read_unsigned_bin(&B, bytes_B, len_B) != MP_OKAY
		|| !H_nn(&u, usr->hash_alg, &usr->A, &B)
		|| !calculate_x(&x, usr->hash_alg, &s, usr->username, usr->password, usr->password_len)
		|| !H_nn(&k, usr->hash_alg, &usr->ng.N, &usr->ng.g))
		goto cleanup_and_exit;

	/* SRP-6a safety check */
	if (!mp_iszero(&B) && !mp_iszero(&u))
	{
		if (mp_exptmod(&usr->ng.g, &x, &usr->ng.N, &v) != MP_OKAY
			/* S = (B - k*(g^x)) ^ (a + ux) */
			|| mp_mul(&u, &x, &tmp1) != MP_OKAY
			|| mp_add(&usr->a, &tmp1, &tmp2) != MP_OKAY // tmp2 = (a + ux)
			|| mp_exptmod(&usr->ng.g, &x, &usr->ng.N, &tmp1) != MP_OKAY
			|| mp_mul(&k, &tmp1, &tmp3) != MP_OKAY // tmp3 = k*(g^x)
			|| mp_sub(&B, &tmp3, &tmp1) != MP_OKAY // tmp1 = (B - K*(g^x))
			|| mp_exptmod(&tmp1, &tmp2, &usr->ng.N, &usr->S) != MP_OKAY
			|| !hash_num(usr->hash_alg, &usr->S, usr->session_key)
			|| !calculate_M(usr->hash_alg, &usr->ng, usr->M, usr->username, &s, &usr->A, &B, usr->session_key)
			|| !calculate_H_AMK(usr->hash_alg, usr->H_AMK, &usr->A, usr->M, usr->session_key))
			goto cleanup_and_exit;

		if (bytes_M)
			*bytes_M = usr->M;

		if (len_M)
			*len_M = hash_length(usr->hash_alg);

		r = true;
	}

cleanup_and_exit:
	mp_clear_multi(&s, &B, &u, &x, &k, &v, &tmp1, &tmp2, &tmp3, NULL);

	return r;
}


bool
srp_user_verify_session(SrpUser *usr, const uint8_t *bytes_HAMK)
{
	bool r = memcmp(usr->H_AMK, bytes_HAMK, hash_length(usr->hash_alg)) == 0;

	if (r)
		usr->authenticated = true;

	return r;
}
