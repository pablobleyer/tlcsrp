/** @file TLCSRP test
*/

#include "tlcsrp.h"

static void
print_user(SrpUser *p)
{
}

static void
print_verifier(SrpVerifier *p)
{
}

static void
print_hex(const uint8_t *d, unsigned l)
{
	unsigned i = 0;

	while (i < l)
	{
		unsigned n = l - i;
		n = (n < 16) ? n : 16;
		printf("%04x: ", i);
		for (unsigned j = 0; j < n; ++j)
			printf ("%02x ", d[i + j]);
		printf("\n");
		i += n;
	}
}

int
main(void)
{
	SrpVerifier ver;
	SrpUser usr;

	const uint8_t *bytes_s = NULL, *bytes_v = NULL, *bytes_A = NULL, *bytes_B = NULL,
			*bytes_M = NULL, *bytes_HAMK = NULL;
	int len_s = 0, len_v = 0, len_A = 0, len_B = 0, len_M = 0, len_HAMK = 0;

	const char username[] = "username";
	const char password[] = "password";

	const char *auth_uname = NULL, *n_hex = NULL, *g_hex = NULL;

	SrpHashAlgorithm alg = srp_SHA512;
	SrpNgType ng_type = srp_Ng_2048;

	int res = 0;

	printf("Username = '%s', Password = '%s'\n", username, password);

	printf("-- srp_create_salted_verification_key --\n");
	bool r = srp_create_salted_verification_key(
		alg, ng_type, username, password, strlen(password),
		&bytes_s, &len_s, &bytes_v, &len_v, n_hex, g_hex
	);
	if (!r)
	{
		res = -1;
		goto cleanup;
	}

	printf("bytes_s[%d]:\n", len_s);
	print_hex(bytes_s, len_s);
	printf("\n");

	printf("bytes_v[%d]:\n", len_v);
	print_hex(bytes_v, len_v);
	printf("\n");

	printf("-- srp_user_init --\n\n");
	r = srp_user_init(&usr, alg, ng_type, username, password, strlen(password), n_hex, g_hex);
	if (!r)
	{
		res = -2;
		goto cleanup;
	}

	printf("-- srp_user_start_authentication --\n");
	r = srp_user_start_authentication(&usr, &auth_uname, &bytes_A, &len_A);
	if (!r)
	{
		res = -3;
		goto cleanup;
	}

	printf("bytes_A[%d]:\n", len_A);
	print_hex(bytes_A, len_A);
	printf("\n");

	// User->Server: (username, bytes_A)
	printf("-- srp_verifier_init --\n");
	r = srp_verifier_init(
		&ver, alg, ng_type, username, bytes_s, len_s, bytes_v, len_v,
		bytes_A, len_A, &bytes_B, &len_B, n_hex, g_hex
	);
	if (!r)
	{
		res = -4;
		goto cleanup;
	}

	printf("bytes_B[%d]:\n", len_B);
	print_hex(bytes_B, len_B);
	printf("\n");

	// Server->User: (bytes_s, bytes_B)
	printf("-- srp_user_process_challenge --\n");
	r = srp_user_process_challenge(&usr, bytes_s, len_s, bytes_B, len_B, &bytes_M, &len_M);
	if (!r)
	{
		res = -5; // User safety check failed
		goto cleanup;
	}

	printf("bytes_M[%d]:\n", len_M);
	print_hex(bytes_M, len_M);
	printf("\n");

	// User->Server: (bytes_M)
	printf("-- srp_verifier_verify_session --\n");
	r = srp_verifier_verify_session(&ver, bytes_M, &bytes_HAMK);
	if (!r)
	{
		res = -6; // User authentication failed
		goto cleanup;
	}

	printf("bytes_HAMK[%d]:\n", len_M);
	print_hex(bytes_HAMK, len_M);
	printf("\n");

	// Server->User: (bytes_HAMK)
	printf("-- srp_user_verify_session --\n");
	r = srp_user_verify_session(&usr, bytes_HAMK);
	if (!r || !srp_user_is_authenticated(&usr))
	{
		res = -7; // Server authentication failed
		goto cleanup;
	}

	printf("User authenticated\n");

cleanup:
	srp_verifier_clear(&ver);
	srp_user_clear(&usr);
	if (bytes_s)
		free((void *)bytes_s);
	if (bytes_v)
		free((void *)bytes_v);

	return res;
}
