/** @file
	CSRP port to TomLib
	@author Pablo Bleyer <pablo@gozendo.com>
*/

/*
 * Secure Remote Password 6a implementation
 * Original copyright:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Zendo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/*
 *
 * Purpose: This is a direct implementation of the Secure Remote Password
 *          Protocol version 6a as described by
 *          http://srp.stanford.edu/design.html
 *
 * Author: tom.cocagne@gmail.com (Tom Cocagne)
 * Modification: pablo@gozendo.com (Pablo Bleyer)
 * Dependencies: TomMath (bignums) and TomCrypt (SHA), and Advapi32.lib on Windows
 *
 * Usage: Refer to test_tlcsrp.c for a demonstration
 *
 * Notes:
 *    This library allows multiple combinations of hashing algorithms and
 *    prime number constants. For authentication to succeed, the hash and
 *    prime number constants must match between
 *    srp_create_salted_verification_key(), srp_user_new(),
 *    and srp_verifier_new(). A recommended approach is to determine the
 *    desired level of security for an application and globally define the
 *    hash and prime number constants to the predetermined values.
 *
 *    As one might suspect, more bits means more security. As one might also
 *    suspect, more bits also means more processing time. The test_srp.c
 *    program can be easily modified to profile various combinations of
 *    hash & prime number pairings.
 */

#ifndef TLCSRP_H_
#define TLCSRP_H_

#include <stdint.h>
#include <stdbool.h>

#include <tommath.h>
#include <tomcrypt.h>

#include <csrp_config.h>

#define SHA1_DIGEST_LENGTH 20
#define SHA224_DIGEST_LENGTH 28
#define SHA256_DIGEST_LENGTH 32
#define SHA384_DIGEST_LENGTH 48
#define SHA512_DIGEST_LENGTH 64

#if defined(__GNUC__)
#define __weak __attribute__((weak))
#elif defined(_MSC_VER)
#define __weak __declspec(selectany)
#endif

typedef enum
{
#ifdef SRP_NG_1024
	srp_Ng_1024,
#endif
#ifdef SRP_NG_1536
	srp_Ng_1536,
#endif
#ifdef SRP_NG_2048
	srp_Ng_2048,
#endif
#ifdef SRP_NG_3072
	srp_Ng_3072,
#endif
#ifdef SRP_NG_4096
	srp_Ng_4096,
#endif
#ifdef SRP_NG_6144
	srp_Ng_6144,
#endif
#ifdef SRP_NG_8192
	srp_Ng_8192,
#endif
	srp_Ng_CUSTOM
} SrpNgType;

typedef enum
{
	srp_SHA1,
	srp_SHA224,
	srp_SHA256,
	srp_SHA384,
	srp_SHA512
} SrpHashAlgorithm;

typedef struct
{
	mp_int N;
	mp_int g;
} SrpNgConstant;

typedef struct SrpVerifier SrpVerifier;
struct SrpVerifier
{
	SrpHashAlgorithm hash_alg;
	SrpNgConstant ng;

	char *username;
	uint8_t *bytes_B;
	bool authenticated;

	uint8_t M[SHA512_DIGEST_LENGTH];
	uint8_t H_AMK[SHA512_DIGEST_LENGTH];
	uint8_t session_key[SHA512_DIGEST_LENGTH];
};

typedef struct SrpUser SrpUser;
struct SrpUser
{
	SrpHashAlgorithm hash_alg;
	SrpNgConstant ng;

	mp_int a;
	mp_int A;
	mp_int S;

	uint8_t *bytes_A;
	bool authenticated;

	char *username;
	char *password;
	int password_len;

	uint8_t M[SHA512_DIGEST_LENGTH];
	uint8_t H_AMK[SHA512_DIGEST_LENGTH];
	uint8_t session_key[SHA512_DIGEST_LENGTH];
};

/**
	Device-dependent function to initialize the random number generator.
	Will be called only once from the library when it succeeds.
*/
/*__weak*/ bool srp_random_init(void);

/* This library will automatically seed the random number generator
 * using cryptographically sound random data on Windows & Linux. If this is
 * undesirable behavior or the host OS does not provide a /dev/urandom file,
 * this function may be called to seed the random number generator with
 * alternate data.
 *
 * The random data should include at least as many bits of entropy as the
 * largest hash function used by the application. So, for example, if a
 * 512-bit hash function is used, the random data requies at least 512
 * bits of entropy.
 *
 * Passing a null pointer to this function will cause this library to skip
 * seeding the random number generator. This is only legitimate if it is
 * absolutely known that the OpenSSL random number generator has already
 * been sufficiently seeded within the running application.
 *
 * Notes:
 *    * This function is optional on Windows & Linux and mandatory on all
 *      other platforms.
 */
void srp_random_seed(const uint8_t *random_data, int data_length);

/* Out: bytes_s, len_s, bytes_v, len_v
 *
 * The caller is responsible for freeing the memory allocated for bytes_s and bytes_v
 *
 * The n_hex and g_hex parameters should be 0 unless SRP_NG_CUSTOM is used for ng_type.
 * If provided, they must contain ASCII text of the hexidecimal notation.
 */
bool
srp_create_salted_verification_key(
	SrpHashAlgorithm alg, SrpNgType ng_type,
	const char *username, const char *password, int len_password,
	const uint8_t **bytes_s, int *len_s, const uint8_t **bytes_v,
	int *len_v, const char *n_hex, const char *g_hex
);

/* Out: bytes_B, len_B.
 *
 * On failure, bytes_B will be set to NULL and len_B will be set to 0
 *
 * The n_hex and g_hex parameters should be 0 unless SRP_NG_CUSTOM is used for ng_type
 */
bool srp_verifier_init(
	SrpVerifier *ver, SrpHashAlgorithm alg, SrpNgType ng_type, const char *username,
	const uint8_t *bytes_s, int len_s, const uint8_t *bytes_v, int len_v,
	const uint8_t *bytes_A, int len_A, const uint8_t **bytes_B, int *len_B,
	const char *n_hex, const char *g_hex
);

void srp_verifier_clear(SrpVerifier *ver);
bool srp_verifier_is_authenticated(SrpVerifier *ver);
const char *srp_verifier_get_username(SrpVerifier *ver);

/* key_length may be null */
const uint8_t *srp_verifier_get_session_key(SrpVerifier *ver, int *key_length);
int srp_verifier_get_session_key_length(SrpVerifier *ver);

/* user_M must be exactly srp_verifier_get_session_key_length() bytes in size */
bool srp_verifier_verify_session(SrpVerifier *ver,
	const uint8_t *user_M, const uint8_t **bytes_HAMK
);

/*******************************************************************************/

/* The n_hex and g_hex parameters should be NULL unless SRP_NG_CUSTOM is used for ng_type */
bool srp_user_init(SrpUser *usr, SrpHashAlgorithm alg, SrpNgType ng_type,
	const char * username, const char *bytes_password, int len_password,
	const char *n_hex, const char *g_hex
);

void srp_user_clear(SrpUser *usr);
bool srp_user_is_authenticated(SrpUser *usr);
const char *srp_user_get_username(SrpUser *usr);

/* key_length may be null */
const uint8_t *srp_user_get_session_key(SrpUser *usr, int *key_length);
int srp_user_get_session_key_length(SrpUser *usr);

/* Output: username, bytes_A, len_A */
bool srp_user_start_authentication(
	SrpUser *usr, const char **username,
	const uint8_t **bytes_A, int *len_A
);

/* Output: bytes_M, len_M  (len_M may be null and will always be
 *                          srp_user_get_session_key_length() bytes in size) */
bool srp_user_process_challenge(SrpUser *usr,
	const uint8_t *bytes_s, int len_s,
	const uint8_t *bytes_B, int len_B,
	const uint8_t **bytes_M, int *len_M
);

/* bytes_HAMK must be exactly srp_user_get_session_key_length() bytes in size */
bool srp_user_verify_session(SrpUser *usr, const uint8_t *bytes_HAMK);

#endif // TLCSRP_H_
