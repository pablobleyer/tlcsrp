tlcsrp
======

Modification of Tom Cocagne's [csrp](https://github.com/cocagne/csrp) for the [LibTom](http://libtom.net) libraries.

csrp is a minimal C implementation of the [Secure Remote Password](http://srp.stanford.edu/) protocol, version SRP-6a.
